const express = require("express");
const app = express();
const path = require("path");
const open = require("open");

const PORT = 8080;

app.use(express.static(path.join(__dirname, "src")));

app.listen(PORT);

open("localhost:" + PORT);
