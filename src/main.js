document.addEventListener("DOMContentLoaded", initApp);

var API_ROOT = "https://jsonplaceholder.typicode.com/";

function initApp() {
  loadUsers();
  USERS_STORE.subscribeOnChange(function(onEventUpdate) {AppRender(onEventUpdate.detail);});
  AppRender(USERS_STORE.getState());
}

function loadUsers() {
  USERS_STORE.sendAction({ type: USERS_STORE_ACTIONS.LOADING_START });

  var url = API_ROOT + "users";

  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if(xhr.readyState !== XMLHttpRequest.DONE){
      return;
    }
    try {
      if(xhr.status !== 200) {
        throw new Error("Getting data fail. Can't get UsersData from server.");
      }
      
      var responseBody = xhr.responseText;
      var bodyData = JSON.parse(responseBody);
      if(!isUsersDataValid(bodyData)) {
        throw new Error("Getting data fail. UsersData is not valid");
      }
      USERS_STORE.sendAction({
        type: USERS_STORE_ACTIONS.LOADING_SUCCESS,
        payload: bodyData,
      });
    } catch(error) {
      console.error(error)
      USERS_STORE.sendAction({
        type: USERS_STORE_ACTIONS.LOADING_ERROR,
        payload: error.message,
      });
    }

  };
  xhr.open("GET", url, true);
  xhr.send();
}

function isUsersDataValid(usersData) {
  // todo (for future)
  return true;
}