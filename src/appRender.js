var USER_FIELDS = {
  NAME: "name",
  USERNAME: "username",
  EMAIL: "email",
  WEBSITE: "website"
};

var usersTableSchema = {
  fieldsOrder: [
    USER_FIELDS.NAME,
    USER_FIELDS.USERNAME,
    USER_FIELDS.EMAIL,
    USER_FIELDS.WEBSITE
  ]
};

function AppRender(state) {
  clearAppContainer();

  if(state.isLoadingError) {
    renderError(state);
    return;
  }

  if (!state.isUsersLoaded) {
    renderLoader(state);
    return;
  }

  renderTable(state);
}

function renderError(state) {
  var error = document.createElement("div");

  var sorry = document.createElement("p");
  sorry.appendChild(document.createTextNode("Something went wrong"));
  error.appendChild(sorry);

  var errorMessage = document.createElement("p");
  errorMessage.appendChild(state.isLoadingError);
  error.appendChild(errorMessage);

  addElementToAppContainer(error);
}

function renderLoader(state) {
  var loader = document.createElement("div");
  loader.appendChild(document.createTextNode("Loading users data..."));

  addElementToAppContainer(loader);
}

function renderTable(state) {

  var header = document.createElement("h1");
  header.appendChild(document.createTextNode("Test task users table"));
  addElementToAppContainer(header);

  var table = createTable(state.users, state.sort);
  addElementToAppContainer(table);
}

function createTable(usersData, sortStatus) {

  var users = sortStatus
    ? sortUsers(usersData, sortStatus)
    : usersData;

  var table = document.createElement("table");
  table.classList.add("UserDataTable")
  var header = createTableHeader(sortStatus);

  table.appendChild(header);

  var body = createTableBody(users);

  table.appendChild(body);

  return table;
}

function sortUsers(users, sortStatus) {
  var sortField = sortStatus.fieldName;

  var sortedUsers = users.sort(function (userA, userB) {
    if (typeof userA[sortField] === "number") {
      return userA[sortField] - userB[sortField];
    } else {
      return userA[sortField].localeCompare(userB[sortField]);
    }
  });

  var sortDirectionReverse = sortStatus.desc;

  return sortDirectionReverse
    ? sortedUsers.reverse()
    : sortedUsers;
}

function createTableHeader(sortStatus) {
  var header = document.createElement("thead");

  var headerRow = document.createElement("tr");

  var headerCells = usersTableSchema.fieldsOrder.map(function(fieldName) {
    return createTableHeaderCell(fieldName, sortStatus);
  });
  var headerCellActions = createTableHeaderCellActions();

  appendAllChildrenToDomElement(headerRow, headerCells);
  headerRow.appendChild(headerCellActions);

  header.appendChild(headerRow);

  return header;
}

function createTableHeaderCell(name, sortStatus) {
  var headerCell = document.createElement("th");
  headerCell.appendChild(document.createTextNode(name));

  var buttonSortContainer = createButtonSortContainer(name, sortStatus);
  headerCell.appendChild(buttonSortContainer);

  return headerCell;
}

function createTableHeaderCellActions() {
  var headerCell = document.createElement("th");
  headerCell.appendChild(document.createTextNode("actions"));

  return headerCell;
}

function createButtonSortContainer(name, sortStatus) {
  var buttonContainer = document.createElement("span");
  buttonContainer.classList.add("UserDataTable__Header__SortButtonsContainer");

  if (sortStatus && sortStatus.fieldName === name) {
    var text = (sortStatus.fieldName === name && !sortStatus.desc)
      ? "sorted asc"
      : "sorted desc";
    buttonContainer.appendChild(document.createTextNode(text));
  }

  var buttonSort = document.createElement("button");
  buttonSort.classList.add("UserDataTable__Header__SortButtonsContainer__SortButton");
  buttonSort.appendChild(document.createTextNode("Sort"));
  buttonSort.addEventListener("click", function () {
    USERS_STORE.sendAction({
      type: USERS_STORE_ACTIONS.SORT,
      payload: {
        fieldName: name,
        desc: (sortStatus && (sortStatus.fieldName === name) && (!sortStatus.desc)),
      },
    });
  });

  buttonContainer.appendChild(buttonSort);

  return buttonContainer;
}

function createTableBody(users) {
  var body = document.createElement("tbody");

  var bodyRows = users.map(function(userData) { return createTableBodyRow(userData);});

  appendAllChildrenToDomElement(body, bodyRows);

  return body;
}

function createTableBodyRow(userData) {
  var bodyRow = document.createElement("tr");

  var bodyCells = usersTableSchema.fieldsOrder.map(function(fieldName) {
    return createTableBodyCell(fieldName, userData);
  });

  appendAllChildrenToDomElement(bodyRow, bodyCells);

  bodyRow.addEventListener("click", function () { showUserDetailModal(userData) })

  var bodyCellActions = createTableBodyCellActions(userData);
  bodyRow.appendChild(bodyCellActions);

  return bodyRow;
}

function createTableBodyCell(fieldName, userData) {
  var cell = document.createElement("td");

  switch(fieldName) {
    case USER_FIELDS.EMAIL:
      var emailFieldData = userData[fieldName];
      var email = document.createElement('a');
      email.appendChild(document.createTextNode(emailFieldData));
      email.title = emailFieldData;
      email.href = "mailto:" + emailFieldData;
      email.addEventListener("click", function(event) { event.stopPropagation(); })
      cell.appendChild(email);
      break;
    case USER_FIELDS.WEBSITE:
      var websiteFieldData = userData[fieldName];
      var website = document.createElement('a');
      website.appendChild(document.createTextNode(websiteFieldData));
      website.title = websiteFieldData;
      website.href = websiteFieldData;
      website.addEventListener("click", function(event) { event.stopPropagation(); })
      cell.appendChild(website);
      break;
    default:
      var userFieldData = userData[fieldName];
      cell.appendChild(document.createTextNode(userFieldData));
      break;
  }

  return cell;
}

function createTableBodyCellActions(userData) {
  var cell = document.createElement("td");

  var actionsContainer = document.createElement("div");

  var deleteUserButton = createDeleteUserButton(userData);
  actionsContainer.appendChild(deleteUserButton);

  cell.appendChild(actionsContainer);
  return cell;
}

function createDeleteUserButton(userData) {
  var button = document.createElement("button");
  button.appendChild(document.createTextNode("Delete"))
  button.addEventListener("click", function (event) {
    event.stopPropagation();
    openDeleteUserConfirmationModal(
      userData,
      function () {
        USERS_STORE.sendAction({
          type: USERS_STORE_ACTIONS.DELETE,
          payload: userData.id,
        });
      }
    )
  });

  return button;
}

function openDeleteUserConfirmationModal(userData, onYes) {
  var container = document.createElement("div");

  var question = document.createElement("h2");
  question.appendChild(document.createTextNode("Do you want to delete user next user?"));
  container.appendChild(question);

  var userName = document.createElement("p");
  userName.appendChild(document.createTextNode(userData.name));
  container.appendChild(userName);

  var notification = document.createElement("p");
  notification.appendChild(document.createTextNode("Notice! This user will be deleted only locally"));
  container.appendChild(notification);

  var buttonsContainer = document.createElement("div");

  var yesButton = document.createElement("button");
  yesButton.appendChild(document.createTextNode("Delete"));
  buttonsContainer.appendChild(yesButton);

  var noButton = document.createElement("button");
  noButton.appendChild(document.createTextNode("Cancel"));
  buttonsContainer.appendChild(noButton);

  container.appendChild(buttonsContainer);

  var modal = ModalRender(container);

  yesButton.addEventListener("click", function () { modal.removeSelf(); onYes() });
  noButton.addEventListener("click", function () { modal.removeSelf(); });


}

function showUserDetailModal(userData) {
  var userDataModal = document.createElement("ul");

  appendAllChildrenToDomElement(userDataModal, [
    createUserDetailLi("street: " + userData.address.street),
    createUserDetailLi("city: " + userData.address.city),
    createUserDetailLi("zipcode: " + userData.address.zipcode),
    createUserDetailLi("company: " + userData.company.name),
  ]);

  ModalRender(userDataModal);
}

function createUserDetailLi(text) {
  var element = document.createElement("li");
  element.appendChild(document.createTextNode(text));

  return element
}
