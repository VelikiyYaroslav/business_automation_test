var USERS_STORE_ACTIONS = {
    LOADING_START: "LOADING_START",
    LOADING_SUCCESS: "LOADING_SUCCESS",
    LOADING_ERROR: "LOADING_ERROR",

    SORT: "SORT",

    DELETE: "DELETE"
}

var INIT_STATE = {
    users: null,
    isUsersLoaded: false,
    isLoadingError: false,
    sort: null,
};

function USERS_STORE_REDUCER(state, action) {
    switch (action.type) {
        case USERS_STORE_ACTIONS.LOADING_START:
            return INIT_STATE;
        case USERS_STORE_ACTIONS.LOADING_SUCCESS:
            return Object.assign({}, state, {
                isUsersLoaded: true,
                users: action.payload,
            });
        case USERS_STORE_ACTIONS.LOADING_ERROR:
            Object.assign({}, state, {
                isLoadingError: action.payload,
            });
        case USERS_STORE_ACTIONS.SORT:
            return  Object.assign({}, state, {
                sort: action.payload,
            });
        case USERS_STORE_ACTIONS.DELETE:
            return Object.assign({}, state, {
                users: state.users.filter(function (user) { return user.id !== action.payload })
            });
        default:
            return state;
    }
}

var USERS_STORE = createState(INIT_STATE, USERS_STORE_REDUCER);
