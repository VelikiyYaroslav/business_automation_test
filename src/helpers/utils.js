function appendAllChildrenToDomElement(element, children) {
    for (var i = 0; i < children.length; i++) {
        element.appendChild(children[i]);
    }
}

function generateUniqueEventName() {
    this.uniqueEventsCount =
        typeof this.uniqueEventsCount === "number" ? this.uniqueEventsCount + 1 : 0;

    var uniqueEventID = this.uniqueEventsCount;

    return "uniqueEvent_" + uniqueEventID;
}

function addElementToAppContainer(element) {
    var appContainer = document.getElementById("App");
    if (!appContainer) {
        console.error("Can't found App container");
        alert("App DOM root element was corrupted. \n Tre to reload page.")
        return;
    }
    appContainer.appendChild(element);
}

function clearAppContainer() {
    var appContainer = document.getElementById("App");
    if (!appContainer) {
        console.error("Can't found app container");
        alert("App DOM root element was corrupted. \n Tre to reload page.")
        return;
    }
    clearDomContainer(appContainer);
}

function clearDomContainer(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}
