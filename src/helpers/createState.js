function createState(initState, reducer) {
  return (function (initState) {
    var state = Object.assign({}, initState);
    var uniqEventName = generateUniqueEventName();
    return {
      getState: function () {
        return Object.assign({}, state);
      },
      subscribeOnChange: function (callback) {
        document.addEventListener(uniqEventName, callback);
      },
      unsubscribeOnChange: function (callback) {
        document.removeEventListener(uniqEventName, callback);
      },
      sendAction: function(action) {
        var nextState = reducer(state, action);
        if(nextState !== state) {
          state = nextState;
          document.dispatchEvent(new CustomEvent(uniqEventName, { detail: state }));
        }
      }
    };
  })(initState);
}
