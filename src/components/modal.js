function ModalRender(modalContentElement) {
    var modalOverlay = createModalOverlay();
    var modalContainer = createModalContainer();

    modalContainer.appendChild(modalContentElement);
    modalOverlay.appendChild(modalContainer);

    var modal = modalOverlay;

    addRemoveFunctionality(modal);

    addElementToAppContainer(modal);

    return modal;
}

function createModalOverlay() {
    var modalOverlay = document.createElement("div");
    modalOverlay.classList.add("Modal__overlay");

    return modalOverlay;
}

function createModalContainer() {
    var modalContainer = document.createElement("div");
    modalContainer.classList.add("Modal__container");

    return modalContainer;
}

function addRemoveFunctionality(modal) {
    function removeModal() {
        modal.remove();
    }
    function onClickEventListener() {
        removeModal();
        removeEventListeners();
    }
    function onEscEventListener(event) {
        if (event.key === "Esc" || "Escape") { // support by all https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
            removeModal();
            removeEventListeners();
            event.preventDefault();
        }
    };
    function removeEventListeners() {
        modal.removeEventListener("click", onClickEventListener);
        document.removeEventListener("keydown", onEscEventListener);
    }
    function removeSelf() {
        removeModal();
        removeEventListeners();
    }
    
    modal.addEventListener("click", onClickEventListener);
    document.addEventListener("keydown", onEscEventListener);
    modal.removeSelf = removeSelf;
}
