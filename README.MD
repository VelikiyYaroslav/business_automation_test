# Using

For run this app follow:

1. clone repo:
```
git clone https://gitlab.com/VelikiyYaroslav/business_automation_test.git
cd business_automation_test
```

2. install packages:
```
npm i
```

3. run app:
```
npm start
```

4. 
> If browser window not opens automatically open page (http://localhost:8080/)[http://localhost:8080/].